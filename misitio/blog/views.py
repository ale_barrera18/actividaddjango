from django.shortcuts import render
from .models import Post
from django.utils import timezone

# Create your views here.
#creamos una funcion post_list

#el "def" es definir,  este caso es definir una funcion 
def post_list(request):
    #El render sirve para mandar objetos del respond a una pagina

    #filter = filtar en la BD
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    #las llaves que estan dentro del render
    #a la vista le estamos mandado el objeto post
    
    return render(request,'blog/post_list.html',{'posts': posts,'var1':'Hola mundo'})
