from django.db import models
#importamos la clase utils el cual contiene timezome
from django.utils import timezone
# Create your models here.
#creamos una clase post, estamos asignado models para reconocerlo en la base de datos
class Post(models.Model):
    #El cascada sirve para eliminar los datos que estan en tablas foraneas. El cascada no es recomendable
    #pero para casos practicos lo utilizaremos.
    #on_delete: sirve cuando se quiere borrar.
    author = models.ForeignKey('auth.user', on_delete=models.CASCADE)
    #El titulo no puede tener mas de 200 caracteres (max_length).
    title = models.CharField(max_length=200)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    #cuando creemos una instacia nueva, se publicara con la fecha nueva y se guardara
    def publish(self):
        self.published_date = timezone.now()
        self.save()
    
    #Cuando la llamemos, solo se escribira el titulo y no toda la informacion que posee.
    def __str__(self):
        return self.title
